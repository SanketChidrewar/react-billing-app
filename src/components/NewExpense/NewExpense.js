import React from "react";
import "./NewExpense.css";
import ExpenseForm from "./ExpenseForm";
const NewExpense = (props) => {
  const SubmitExpenseHandler = (submittedExpense) => {
    const expense = { ...submittedExpense, id: Math.random().toString() };
    console.log(expense);
    props.onAddingNewExpense(expense);
  };
  return (
    <div className="new-expense">
      <ExpenseForm onSubmitExpense={SubmitExpenseHandler} />
    </div>
  );
};

export default NewExpense;

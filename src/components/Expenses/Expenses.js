import ExpenseItem from "./ExpenseItem";
import { useState } from "react";
import ExpensesFilter from "./ExpensesFilter";
import "./Expenses.css";
import ExpenseChart from "./ExpenseChart";
import Card from "../UI/Card";
function Expenses(props) {
  const { expenses } = props;
  const [selectedYear, setSelectedYear] = useState(2019);

  const handleFilterSelect = (year) => {
    console.log("Expenses.js");
    console.log(year);
    setSelectedYear(year);
  };

  const filteredExpenses = expenses.filter((exp) => {
    return exp.date.getFullYear().toString() === selectedYear;
  });

  return (
    <Card className="expenses">
      <ExpensesFilter
        onFilterSelect={handleFilterSelect}
        selected={selectedYear}
      />
      <ExpenseChart expenses={filteredExpenses} />
      {filteredExpenses.length === 0 ? (
        <p style={{ color: "white" }}>Found No Expenses</p>
      ) : (
        filteredExpenses.map((expense) => {
          return (
            <ExpenseItem
              key={expense.id}
              title={expense.title}
              amount={expense.amount}
              date={expense.date}
            ></ExpenseItem>
          );
        })
      )}
    </Card>
  );
}
export default Expenses;

import "./ExpenseItem.css";
import "./ExpenseDate.css";
import { useState } from "react";
import ExpenseDate from "./ExpenseDate";

function ExpenseItem(props) {
  const [title, setTitle] = useState(props.title);
  // console.log("Expense Item evaluated by react");
  const clickHandler = () => {
    setTitle("Clicked");
    console.log(title);
  };
  return (
    <div className="expense-item">
      <ExpenseDate date={props.date}></ExpenseDate>
      <div className="expense-item__description">
        <h2>{title}</h2>
        <div className="expense-item__price">{props.amount}</div>
        <button onClick={clickHandler}>Click here</button>
      </div>
    </div>
  );
}

export default ExpenseItem;

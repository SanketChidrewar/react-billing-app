import { useState } from "react";

import Expenses from "./components/Expenses/Expenses";

import NewExpense from "./components/NewExpense/NewExpense";
const DUMMY_EXPENSES = [
  { id: 1, title: "Toilet Paper", amount: 800, date: new Date(2021, 2, 12) },
  { id: 2, title: "New TV", amount: 21000, date: new Date(2020, 3, 10) },
  { id: 3, title: "Chair", amount: 5000, date: new Date(2021, 4, 12) },
  { id: 4, title: "Bike", amount: 150000, date: new Date(2021, 4, 13) },
];
function App() {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);
  const AddingNewExpenseHandler = (expense) => {
    console.log(expense);
    // setExpenses([...expenses, expense]);
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };

  return (
    <div>
      <NewExpense onAddingNewExpense={AddingNewExpenseHandler} />
      <Expenses expenses={expenses} />
    </div>
  );
}

export default App;
